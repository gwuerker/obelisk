''' Obelisk - Job '''

import os

def createSubmitFile(job_id):
    submit_file = open('job/{}/submit.sh'.format(job_id), 'w+')
    submit_file.write("""
        #!/bin/bash -l
        ###############
        # PBS Options #
        ###############

        # Email notifications
        #PBS -m abe
        #PBS -M example@uwec.edu

        #Name of the job (cosmetic)
        #PBS -N first-test

        # Job Queue
        #PBS -q batch

        # Default working directory
        #PBS -d /data/users/wuerkegr/orca-example

        # Request resources and set maximum runtime
        #PBS -l nodes=1:ppn=1,walltime=24:00:00

        # Job Script
        module load qchem
    """)
    submit_file.close()


