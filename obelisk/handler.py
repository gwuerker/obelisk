''' Obelisk - Handlers '''

import tornado.web
import subprocess
import random
import os

# Job Handler

class JobHandler(tornado.web.RequestHandler):
    def post(self, job_id=None):
        program = self.get_argument('program', None)
        self.write('Program is {}'.format(program))
        '''
        job_id = next(self.application.job_id)
        os.makedirs('job/%id' % job_id)
        createSubmitFile(job_id)
        return job
        '''

# Wizard Handler

class WizardHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('wizard.tmpl', programs=self.application.programs)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
