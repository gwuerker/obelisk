''' Obelisk - Application '''

import collections
import logging
import os

import tornado.ioloop
import tornado.web

from handler import JobHandler, WizardHandler

# Obelisk Application

class ObeliskApplication(tornado.web.Application):
    HTTP_PORT = 9608

    def __init__(self, port=None, **settings):
        settings.update({
            'debug'         : True,
            'template_path' : os.path.join(os.path.dirname(__file__), 'templates'),
        })
        tornado.web.Application.__init__(self, **settings)

        self.port          = port or self.HTTP_PORT
        self.search_ports  = port is self.HTTP_PORT
        self.ioloop        = tornado.ioloop.IOLoop.instance()
        self.logger        = logging.getLogger()
        self.job_id        = collections.Counter()
        self.workspace     = os.curdir
        self.programs      = ['qchem', 'uname']

        self.add_handlers('', [
            (r'/job/([\d]*)'    , JobHandler),
            (r'/'               , WizardHandler),
        ])

    def run(self):
        self.listen(self.port, xheaders=True)
        self.ioloop.start()

# vim: sts=4 sw=4 ts=8 expandtab ft=python
