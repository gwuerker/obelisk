#!/usr/bin/env python2.7

''' Obelisk - Main '''

import sys
    
from tornado.options    import define, options
from application        import ObeliskApplication

# Main execution

if __name__ == '__main__':
    define('port', default=ObeliskApplication.HTTP_PORT, help='HTTP Port to listen on')

    options.parse_command_line()

    try:
        obelisk_application = ObeliskApplication(port=options.port)
        obelisk_application.run()
    except KeyboardInterrupt:
        sys.exit(0)

# vim: sts=4 sw=4 ts=8 expandtab ft=python
